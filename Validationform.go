package validationform

import (
	"encoding/json"
	"fmt"
	"strings"
)

/** Function Validation Token And BodyRow request
* Reqired Function bodyRowmandatory()
* Create By ptr.nov@gmail.com
* date 16 Mey 2020
* Ver 1.0.0
 */
func Bodyrow(m string, key string) (err string, msg string, data map[string]interface{}) {

	err = "success"
	msg = "request validation success"

	/* BODY ROWS MANIPULATION*/
	readerRawBody := strings.NewReader(m)

	//--Json Minify
	dec := json.NewDecoder(readerRawBody)
	var rstBodyRow map[string]interface{}
	dec.Decode(&rstBodyRow)

	//-- result data json
	data = rstBodyRow

	/* KEY MANDATORY  MANIPULATION*/
	readerKey := strings.NewReader(key)

	//--Json Minify
	decKey := json.NewDecoder(readerKey)
	var rsltKey map[string]interface{}
	decKey.Decode(&rsltKey)

	/* Code Validate Key & Value kosong*/
	if rstBodyRow != nil {
		for k1, v1 := range rsltKey {
			for k2, v2 := range rstBodyRow {
				fmt.Println("header check :", v1)
				ck := _bodyRowmandatory(rstBodyRow, fmt.Sprint(v1))
				if ck == true {
					//--> key required true
					if v2 == "" {
						//--> key is empty
						err = "failed"
						msg = "required " + k2 + " value"
						break
					}
				} else {
					err = "failed"
					msg = fmt.Sprint(v1) + " is key required"
					fmt.Printf("%v %v", k1, v1)
					break
				}
			}

		}
	} else {
		err = "failed"
		msg = "No data request"
	}
	return
}

func _bodyRowmandatory(v map[string]interface{}, key string) (err bool) {
	for k1, v1 := range v {
		if k1 == key {
			err = true
			break
		} else {
			err = false
		}
		fmt.Printf("%v %v", k1, v1)
	}
	return
}
